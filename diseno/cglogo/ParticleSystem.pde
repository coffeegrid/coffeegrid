import java.util.Iterator;

class ParticleSystem {
  ArrayList<Particle> particles;
  PVector origin;

  ParticleSystem() {
    particles = new ArrayList<Particle>();
  }

  void addParticle(PVector location) {
    particles.add(new Particle(location));
  }

  void run() {
    Iterator<Particle> it = particles.iterator();
    while (it.hasNext()) {
      Particle p = it.next();
      p.run();
    }
  }
  
  int count() {
    return particles.size();
  }
  
  void clear() {
    particles.clear();
  }
}

