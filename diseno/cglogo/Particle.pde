class Particle {
  PVector location;
  PVector velocity;
  PVector acceleration;
  float lifespan;

  Particle(PVector l) {
    acceleration = new PVector(0,0.1);
    velocity = new PVector(random(-0.1,0.1),random(0,0.3));
    location = l.get();
  }

  void run() {
    update();
    display();
  }

  // Method to update location
  void update() {
    velocity.add(acceleration);
    if(abs(velocity.y) > 0.1 && location.y < height - 4) {
      location.add(velocity);
    }
    if(location.y > height - 24) {
      velocity.y = -velocity.y * 0.5;
      velocity.x = velocity.x * 0.9;
    }
    if(location.x > width - 24 || location.x < 0 -16) {
      velocity.x = -velocity.x;
    }
  }

  // Method to display
  void display() {
    noStroke();
    fill(255);
    ellipse(location.x,location.y,4,4);
  }
}

