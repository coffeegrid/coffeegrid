/*
*  CoffeeGrid logo using bezier curves
*  Press 'i' to compare with original image
*  Press 'g' to see guides
*/


PImage logoImage;
Logo logo = new Logo();

boolean showImage = false;
boolean showGuides = false;

final int WIDTH = 120;
final int HEIGHT = 120;

void setup() {
  logoImage = loadImage("coffeegrid_logo_black.png");
  size(WIDTH,HEIGHT); 
  noFill();
}

void draw() {
  translate(20,20);
  background(0);
  if (showImage) {
    image(logoImage,0,0);
  }
  
  logo.drawLogo(false);
  
  if (showGuides) {
    stroke(255);
    strokeWeight(1);
    line(0,mouseY, WIDTH, mouseY);
    line(mouseX, 0, mouseX, HEIGHT);
  }  
}

void mousePressed() {
  logo.dropBallsToggle();
}

void keyPressed() {
  if (key == 'i') {
     showImage = !showImage; 
  }
  if (key == 'g') {
    showGuides = !showGuides;
  }
}



