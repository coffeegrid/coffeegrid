class Logo {
  final int[][] coords = {
    {35,25,25,12,27,8,10,9}, // 1
    {7,44,-2,11,0,7,15,9}, // 2
    {7,44,17,76,11,71,39,72}, // 3
    {72,44,61,76,68,71,40,72}, // 4
    {69,9,81,8,81,10,72,44}, // 5
    {40,33,59,8,55,9,68,9}, // 6
    {40,33,19,61,23,55,39,56}, // 7
    {45,40,62,61,54,55,40,56} // 8
  };
  
  int lastTime = 0;
  
  float[] xc;
  float[] yc;
  
  boolean dropping = false;

  void drawLogo(boolean lines) {
    if (!dropping) {
      float steps = ((float(mouseX)/float(width))*40.0)+5;
      xc = new float[(int(steps)+1)*8];
      yc = new float[(int(steps)+1)*8];
      for (int i =0 ; i < coords.length; i++) {
        int[] crd = coords[i];
        if (lines) {
          stroke(255,0,0);
          strokeWeight(1);
          line(crd[0],crd[1],crd[2],crd[3]);
          line(crd[4],crd[5],crd[6],crd[7]);
        }
  //      stroke(255);
        strokeWeight(4);
        bezier(crd[0],crd[1],crd[2],crd[3],crd[4],crd[5],crd[6],crd[7]);
        for (int j = 0; j <= steps; j++) {
          float t = j / steps;
          float x = bezierPoint(crd[0], crd[2], crd[4], crd[6], t);
          float y = bezierPoint(crd[1], crd[3], crd[5], crd[7], t);
          fill(255);
          noStroke();
          ellipse(x, y, 4, 4);
          xc[(i*int(steps))+j] =x;
          yc[(i*int(steps))+j] =y;
        }
        noFill();
      }
    } else {
//      noStroke();
//      fill(255);
//
//      for(int i = 0; i < xc.length; i++) {
//        ellipse(xc[i],yc[i],4,4);
//      }
    ps.run();

    }
  }
  
  ParticleSystem ps = new ParticleSystem();
  
  void dropBallsToggle() {
    dropping = !dropping;
    if (dropping) {
      for(int i = 0; i < yc.length; i++) {
          ps.addParticle(new PVector(xc[i],yc[i]));
      }
    } else {
      ps.clear();
    }
  }
}

