var coords = [
	      [35,25,25,12,27,8,10,9], // 1
	      [7,44,-2,11,0,7,15,9], // 2
	      [7,44,17,76,11,71,39,72], // 3
	      [72,44,61,76,68,71,40,72], // 4
	      [69,9,81,8,81,10,72,44], // 5
	      [40,33,59,8,55,9,68,9], // 6
	      [40,33,19,61,23,55,39,56], // 7
	      [45,40,62,61,54,55,40,56] // 8
	      ];

var scrollLifespan = 24;

function sketchProc(processing) {
    processing.setup = function() {
        processing.frameRate(24); 
	processing.size(120,120);
        processing.noLoop();
    }
    
    processing.draw = function() {
        processing.background(0);
        processing.translate(20,20);
        drawLogo();
        --scrollLifespan;
        if (scrollLifespan <= 0) {
            processing.noLoop();
        }
    };

    function drawLogo() {
        var maxScroll = document.body.scrollHeight - window.innerHeight;
        var steps = (((maxScroll-window.scrollY)/maxScroll)*40.0)+5;
        for (var i = 0 ; i < coords.length; i++) {
            var crd = coords[i];
            for (var j = 0; j <= steps; j++) {
		var t = j / steps;
		var x = processing.bezierPoint(crd[0], crd[2], crd[4], crd[6], t);
		var y = processing.bezierPoint(crd[1], crd[3], crd[5], crd[7], t);
		processing.fill(255);
		processing.noStroke();
		processing.ellipse(x, y, 4, 4);
            }
        }
    }
    
    window.onscroll = function() {
        scrollLifespan = 24;
        processing.loop();   
    }
}

var canvas = document.getElementById("logocanvas");
var p = new Processing(canvas, sketchProc);
