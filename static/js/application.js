// Some general UI pack related JS

$(document).ready(function() {
    

    $("a[data-scroll='true']").click(function(){
        $("html, body").animate({scrollTop: $($(this).attr("href")).offset().top}, 600);
        return false;
    });

  var $navbar = $(".navbar");
  var triger = $navbar.offset().top;
  var $brand = $("a.brand");
  $brand.hide();
  $(window).scroll(function(){
      var scroll_top = $(window).scrollTop();    
      
      if (scroll_top >= triger){
	  console.log("scroll >");
	  $navbar.addClass("navbar-fixed-top");
	  $brand.show("slow");
      }

      if (scroll_top < triger){
	  console.log("scroll <");
	  $navbar.removeClass("navbar-fixed-top");
	  $brand.hide("slow");
      }
      
  });
});


//$(function () {
    // Custom selects
//  $("select").dropkick();
//});

$(document).ready(function() {
    // Todo list
    $(".todo li").click(function() {
        $(this).toggleClass("todo-done");
    });

    // Init tooltips
    //    $("[data-toggle=tooltip]").tooltip("show");

    // Init tags input
    //    $("#tagsinput").tagsInput();

    // Init jQuery UI slider
    //    $("#slider").slider({
    //        min: 1,
    //        max: 5,
    //        value: 2,
    //        orientation: "horizontal",
    //        range: "min",
    //    });

    // JS input/textarea placeholder
    //    $("input, textarea").placeholder();

    // Make pagination demo work
    $(".pagination a").click(function() {
        if (!$(this).parent().hasClass("previous") && !$(this).parent().hasClass("next")) {
            $(this).parent().siblings("li").removeClass("active");
            $(this).parent().addClass("active");
        }
    });

    $(".btn-group a").click(function() {
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
    });

    // Disable link click not scroll top
    $("a[href='#']").click(function() {
        return false
    });

});






// PEOPLE PAGINATION

$(document).ready(function() {
	$('#people .pagination li.previous a').click(previousPage);
	$('#people .pagination li.next a').click(nextPage);
	$('#people .pagination li.pager-number a').click(function() {
		var page = parseInt($(this).html());
		switchToPage(page);
	    });
});

function nextPage() {
    var $pagerItem = $('.pagination ul li.active', '#people');
    var activePage = parseInt($pagerItem.find('a').html());
    if (activePage < 3) {
	switchToPage(activePage + 1);
    }
}

function previousPage() {
    var $pagerItem = $('.pagination ul li.active', '#people');
    var activePage = parseInt($pagerItem.find('a').html());
    if (activePage > 1) {
	switchToPage(activePage - 1);
    }
}

function switchToPage(page) {
    $('.page', '#people').hide();
    $('#page' + page, '#people').show();
    setActivePage(page);
}

function setActivePage(page) {
    var $paginator = $('#people .pagination');
    $paginator.find('li').removeClass('active');
    var $el = $paginator.find('#nav-page' + page);
    $el.addClass('active');
}
